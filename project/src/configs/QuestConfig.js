"use strict";

module.exports = {
    "redeemTime": 48,
    "repeatableQuests": [
        {
            "name": "Daily",
            "types": ["Elimination", "Completion", "Exploration"],
            "resetTime": 60 * 60 * 24,
            "numQuests": 3,
            "minPlayerLevel": 5,
            "rewardScaling": {
                "levels": [1, 20, 45, 100],
                "experience":  [2000, 4000, 20000, 80000],
                "roubles": [6000, 10000, 100000, 250000],
                "items": [1, 2, 4, 4],
                "reputation": [0.01, 0.01, 0.01, 0.01],
                "rewardSpread": 0.5 // spread for factor of reward at 0.5 the reward according to level is multiplied by a random value between 0.5 and 1.5
            },
            "locations": {
                "any": ["any"],
                "factory4_day": ["factory4_day", "factory4_night"],
                "bigmap": ["bigmap"],
                "Woods": ["Woods"],
                "Shoreline": ["Shoreline"],
                "Interchange": ["Interchange"],
                "Lighthouse": ["Lighthouse"],
                "laboratory": ["laboratory"],
                "RezervBase": ["RezervBase"]
            },
            "questConfig": {
                "Exploration": {
                    "maxExtracts": 3,
                    "specificExits": {
                        "probability": 0.25,
                        "passageRequirementWhitelist": [
                            "None",
                            "TransferItem",     // car extracts
                            "WorldEvent",       // activate trigger condition
                            "Train",
                            "Reference",        // special stuff like cliff descent
                            "Empty"             // no backpack
                        ]                       // currently all but "ScavCooperation"
                    }
                },
                "Completion": {
                    "minRequestedAmount": 1,
                    "maxRequestedAmount": 5,
                    "minRequestedBulletAmount": 20,
                    "maxRequestedBulletAmount": 60,
                    "useWhitelist": true,
                    "useBlacklist": false,
                },
                "Elimination": {
                    "targets": new RandomUtil.ProbabilityObjectArray(
                        new RandomUtil.ProbabilityObject("Savage", 7, { "isBoss": false }),
                        new RandomUtil.ProbabilityObject("AnyPmc", 2, { "isBoss": false }),
                        new RandomUtil.ProbabilityObject("bossBully", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossGluhar", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossKilla", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossSanitar", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossTagilla", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossKojaniy", 0.5, { "isBoss": true }),
                    ),
                    "bodyPartProb": 0.4,
                    "bodyParts": new RandomUtil.ProbabilityObjectArray(
                        new RandomUtil.ProbabilityObject("Head", 1, ["Head"]),
                        new RandomUtil.ProbabilityObject("Stomach", 3, ["Stomach"]),
                        new RandomUtil.ProbabilityObject("Chest", 5, ["Chest"]),
                        new RandomUtil.ProbabilityObject("Arms", 0.5, ["LeftArm", "RightArm"]),
                        new RandomUtil.ProbabilityObject("Legs", 0.5, ["LeftLeg", "RightLeg"]),
                    ),
                    "specificLocationProb": 0.25,
                    "distLocationBlacklist": ["laboratory", "factory4_day", "factory4_night"],
                    "distProb": 0.25,
                    "maxDist": 200,
                    "minDist": 20,
                    "maxKills": 5,
                    "minKills": 2
                }
            }
        },
        {
            "name": "Weekly",
            "types": ["Elimination", "Completion", "Exploration"],
            "resetTime": 7 * 60 * 60 * 24,
            "numQuests": 1,
            "minPlayerLevel": 15,
            "rewardScaling": {
                "levels": [1, 20, 45, 100],
                "experience":  [4000, 8000, 40000, 160000],
                "roubles": [12000, 20000, 200000, 500000],
                "items": [3, 3, 4, 4],
                "reputation": [0.02, 0.03, 0.03, 0.03],
                "rewardSpread": 0.5 // spread for factor of reward at 0.5 the reward according to level is multiplied by a random value between 0.5 and 1.5
            },
            "locations": {
                "any": ["any"],
                "factory4_day": ["factory4_day", "factory4_night"],
                "bigmap": ["bigmap"],
                "Woods": ["Woods"],
                "Shoreline": ["Shoreline"],
                "Interchange": ["Interchange"],
                "Lighthouse": ["Lighthouse"],
                "laboratory": ["laboratory"],
                "RezervBase": ["RezervBase"]
            },
            "questConfig": {
                "Exploration": {
                    "maxExtracts": 10,
                    "specificExits": {
                        "probability": 0.5,
                        "passageRequirementWhitelist": [
                            "None",
                            "TransferItem",     // car extracts
                            "WorldEvent",       // activate trigger condition
                            "Train",
                            "Reference",        // special stuff like cliff descent
                            "Empty"             // no backpack
                        ]                       // currently all but "ScavCooperation"
                    }
                },
                "Completion": {
                    "minRequestedAmount": 2,
                    "maxRequestedAmount": 10,
                    "minRequestedBulletAmount": 20,
                    "maxRequestedBulletAmount": 60,
                    "useWhitelist": true,
                    "useBlacklist": false,
                },
                "Elimination": {
                    "targets": new RandomUtil.ProbabilityObjectArray(
                        new RandomUtil.ProbabilityObject("Savage", 5, { "isBoss": false }),
                        new RandomUtil.ProbabilityObject("AnyPmc", 5, { "isBoss": false }),
                        new RandomUtil.ProbabilityObject("bossBully", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossGluhar", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossKilla", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossSanitar", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossTagilla", 0.5, { "isBoss": true }),
                        new RandomUtil.ProbabilityObject("bossKojaniy", 0.5, { "isBoss": true }),
                    ),
                    "bodyPartProb": 0.4,
                    "bodyParts": new RandomUtil.ProbabilityObjectArray(
                        new RandomUtil.ProbabilityObject("Head", 1, ["Head"]),
                        new RandomUtil.ProbabilityObject("Stomach", 1, ["Stomach"]),
                        new RandomUtil.ProbabilityObject("Chest", 1, ["Chest"]),
                        new RandomUtil.ProbabilityObject("Arms", 0.5, ["LeftArm", "RightArm"]),
                        new RandomUtil.ProbabilityObject("Legs", 0.5, ["LeftLeg", "RightLeg"]),
                    ),
                    "specificLocationProb": 0.25,
                    "distLocationBlacklist": ["laboratory", "factory4_day", "factory4_night"],
                    "distProb": 0.25,
                    "maxDist": 200,
                    "minDist": 20,
                    "maxKills": 15,
                    "minKills": 5
                }
            }
        },
    ]
};

// Request for dailies has been according to wiki:
// level 45+
// add reward randomistion:
// 20,000 to 80,000 exp
// 100,000 to 250,000 roubles
// 700 to 1750 euros if from peacekeeper
// 1 to 4 items
//
// level 21-45
// add reward randomistion:
// up to 20,000 exp
// up to 100,000 roubles
// up to 700 if from peacekeeper
// 1 to 4 items
//
// level 5-20
// add reward randomistion:
// up to 2000 exp
// up to 10,000 roubles
// up to 70 if from peacekeeper
// 1 to 2 items
//
// quest types:
// exit location
// extract between 1 and 5 times from location
//
// elimination PMC
// kill between 2-15 PMCs
// from a distance between 20-50 meters
// kill via damage from a particular body part
//
// elimination scav
// kill between 2-15 scavs
// from a distance between 20-50 meters
// kill via damage from a particular body part
//
// boss elimination
// any distance OR from a distance of more than 80
//
// find and transfer
// find and handover a random number of items
// items are random
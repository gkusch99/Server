"use strict";

require("../Lib.js");

class DurabilityLimitsHelper
{
    static getRandomisedMaxWeaponDurability(itemTemplate, botRole)
    {
        if (botRole && BotController.isBotPmc(botRole))
        {
            return itemTemplate._props.MaxDurability;
        }

        if (botRole && botRole === "pmcbot" || botRole === "exusec")
        {
            return RandomUtil.getInt((itemTemplate._props.MaxDurability * 0.9), itemTemplate._props.MaxDurability);
        }

        if (botRole && (BotController.isBotBoss(botRole) || BotController.isBotFollower(botRole)))
        {
            return RandomUtil.getInt(itemTemplate._props.durabSpawnMax, itemTemplate._props.MaxDurability);
        }

        return RandomUtil.getInt(itemTemplate._props.durabSpawnMin, itemTemplate._props.durabSpawnMax);
    }

    static getRandomisedWeaponDurability(itemTemplate, botRole, maxDurability)
    {
        if (botRole && (BotController.isBotPmc(botRole)))
        {
            // e.g. min: 100(max) * 0.8(min) = 80
            return RandomUtil.getInt((maxDurability * DurabilityLimitsHelper.getMinWeaponDurabilityFromConfig("pmc")), maxDurability);
        }

        if (botRole && (BotController.isBotBoss(botRole) || BotController.isBotFollower(botRole)))
        {
            // ensure min isnt above max
            const configMin = maxDurability * DurabilityLimitsHelper.getMinWeaponDurabilityFromConfig("boss");
            const minDurability = configMin > maxDurability ? maxDurability : configMin;
            return RandomUtil.getInt(minDurability, maxDurability);
        }

        if (botRole && DurabilityLimitsHelper.getMinWeaponDurabilityFromConfig(botRole))
        {
            return RandomUtil.getInt((maxDurability * DurabilityLimitsHelper.getMinWeaponDurabilityFromConfig(botRole)), maxDurability);
        }

        return RandomUtil.getInt(itemTemplate._props.durabSpawnMin, maxDurability);
    }

    static getRandomisedArmorDurability(itemTemplate, botRole, maxDurability)
    {
        if (botRole && (BotController.isBotPmc(botRole)))
        {
            // e.g. min: 100(max) * 0.8(min) = 80
            return RandomUtil.getInt((maxDurability * DurabilityLimitsHelper.getMinArmorDurabilityFromConfig("pmc")), maxDurability);
        }

        if (botRole && (BotController.isBotBoss(botRole) || BotController.isBotFollower(botRole)))
        {
            return RandomUtil.getInt(maxDurability * DurabilityLimitsHelper.getMinArmorDurabilityFromConfig("boss"), maxDurability);
        }

        if (botRole && DurabilityLimitsHelper.getMinWeaponDurabilityFromConfig(botRole))
        {
            // e.g. min: 100(max) * 0.8(min) = 80
            return RandomUtil.getInt((maxDurability * DurabilityLimitsHelper.getMinArmorDurabilityFromConfig(botRole)), maxDurability);
        }

        return RandomUtil.getIntEx(maxDurability);
    }

    /** Convert from percent to decimal */
    static getMinWeaponDurabilityFromConfig(role)
    {
        if (BotConfig.durability[role])
        {
            return BotConfig.durability[role].weapon.minPercent / 100;
        }

        return undefined;
    }

    /** Convert from percent to decimal */
    static getMinArmorDurabilityFromConfig(role)
    {
        if (BotConfig.durability[role])
        {
            return BotConfig.durability[role].armor.minPercent / 100;
        }

        return undefined;
    }
}

module.exports = DurabilityLimitsHelper;